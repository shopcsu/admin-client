import Item from '~/plugins/api/models/Item'
import Category from '~/plugins/api/models/Category'
import Login from '~/plugins/api/models/Login'
import Order from '~/plugins/api/models/Order'

export class ApiClass {
  constructor ({ client, store }) {
    this._client = client
    this.$store = store
  }

  get $client () {
    if (this.$store.getters.isAuthorized) {
      this._client.setToken(`Bearer ${this.$store.getters.authToken}`)
    }
    return this._client
  }

  async login (model) {
    return await this.$client.$post('/auth', new Login(model))
  }

  async itemList () {
    return (await this.$client.$get('/admin/items')).map(e => new Item(e))
  }

  async createItem (model) {
    return await this.$client.$post('/admin/items', new Item(model))
  }

  async readItem (id) {
    return new Item(await this.$client.$get(`/admin/items/${id}`))
  }

  async updateItem (id, model) {
    return await this.$client.$post(`/admin/items/${id}`, new Item(model))
  }

  deleteItem (id) {
    return this.$client.$delete(`/admin/items/${id}`)
  }

  async categoryList () {
    return (await this.$client.$get('/admin/categories')).map(e => new Category(e))
  }

  async createCategory (model) {
    return await this.$client.$post('/admin/categories', new Category(model))
  }

  async readCategory (id) {
    return new Category(await this.$client.$get(`/admin/categories/${id}`))
  }

  async updateCategory (id, model) {
    return await this.$client.$post(`/admin/categories/${id}`, new Category(model))
  }

  deleteCategory (id) {
    return this.$client.$delete(`/admin/categories/${id}`)
  }

  async orderList () {
    return (await this.$client.$get('/orders')).map(e => new Order(e))
  }

  async cancelOrder (id) {
    return (await this.$client.$patch(`/orders/canceled/${id}`))
  }

  async confirmOrder (id) {
    return (await this.$client.$patch(`/orders/confirmed/${id}`))
  }

  async finishOrder (id) {
    return (await this.$client.$patch(`/orders/finished/${id}`))
  }
}
