import { ActiveModel } from '@alt-point/active-models'

export default class Category extends ActiveModel {
  static get fillable () {
    return [
      'id', 'title', 'parentId'
    ]
  }

  static get $attributes () {
    return {
      id: 0,
      title: '',
      parentId: null
    }
  }
}
