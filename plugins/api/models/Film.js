import { ActiveModel } from '@alt-point/active-models'

export default class Film extends ActiveModel {
  static get fillable () {
    return [
      'id',
      'title',
      'original_title',
      'original_title_romanised',
      'description',
      'director',
      'producer',
      'release_date',
      'running_time',
      'rt_score',
      'url'
    ]
  }

  static get $attributes () {
    return {
      id: 0,
      title: '',
      original_title: '',
      original_title_romanised: '',
      description: '',
      director: '',
      producer: '',
      release_date: '',
      running_time: '',
      rt_score: '',
      url: ''
    }
  }
}
