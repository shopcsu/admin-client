import { ActiveModel } from '@alt-point/active-models'

export default class Item extends ActiveModel {
  static get fillable () {
    return ['id', 'categoryId', 'title', 'price', 'picture', 'description']
  }

  static get $attributes () {
    return {
      id: 0,
      categoryId: 0,
      title: '',
      price: 0,
      picture: '',
      description: ''
    }
  }
}
