import { ActiveModel } from '@alt-point/active-models'

export default class Authorization extends ActiveModel {
  static get fillable () {
    return ['authToken', 'refreshToken', 'expiresAt']
  }

  static get $attributes () {
    return {
      authToken: '',
      refreshToken: '',
      expiresAt: ''
    }
  }
}
