FROM node:lts-alpine

# параметры проекта
ARG API_USE_HTTPS
ARG API_PORT
ARG API_HOST

ENV API_USE_HTTPS=$API_USE_HTTPS
ENV API_PORT=$API_PORT
ENV API_HOST=$API_HOST

# устанавливаем простой HTTP-сервер для статики
RUN npm install -g http-server

# делаем каталог 'app' текущим рабочим каталогом
WORKDIR /app

# копируем оба 'package.json' и 'package-lock.json' (если есть)
COPY package*.json ./
COPY package-lock*.json ./

# устанавливаем зависимости проекта
RUN npm install

# копируем файлы и каталоги проекта в текущий рабочий каталог (т.е. в каталог 'app')
COPY . .

# собираем приложение для production с минификацией
RUN npm run build
RUN npm run generate

EXPOSE 8080
CMD [ "http-server", "dist" ]
